# GDocExporter (1.0.7)

![doc.png](readme_images/doc.png) ![azure.png](readme_images/azure.png)

![net.png](readme_images/net.png)

GDocExporter is a highly targeted application that allows you to parse Google documents and create corresponding tasks in AureDevops.

### What implemented in 1.0.7:
1. Task export/import
2. Acceptance critera parsing
3. Work item's settings (you can set target branch, tag, activity type, area and iteration)
4. Total estimate calculation and setting of PBI effort, start week and end week
5. Tasks preview (you can preview exported tasks and and choose which ones to import)
6. Automatic assignment of tasks

### Installation

- You can clone repository and build application on your machine
- Or you can download builded application: https://drive.google.com/drive/folders/1TK5vt9y_jW4RRY2LKe41jX0vLFMV_iGr?usp=sharing

### Requirements

GDocExporter requires [AzureCLI](https://docs.microsoft.com/ru-ru/cli/azure/install-azure-cli-windows?tabs=azure-cli) to run.

To check that AzureCLI is installed run the command.

```sh
az --version
```

### Run

#### Export Google Document

1. Copy ID of your's Google document 
2. Run GDocExporter.exe 
3. Insert ID of your's Google document  to 'Goggle Document identifier' field
4. Press 'Export'

![export.png](readme_images/export.png)

![id.png](readme_images/id.png)

#### Import to Azure

1. Fill 'Work item's settings' tab
2. Select the tasks you want to import using checkboxes
3. Change effort values if it needed
4. Press 'Import'

![settings.png](readme_images/settings.png)

![import.png](readme_images/import.png)

### Google document structure

Template: https://docs.google.com/document/d/1LmARcepMYsRvPflysvo0OXX2cpso6gE3pHsfsVgs5TM/edit?usp=sharing

The general structure of the document
1. Title
2. Acceptance critera
3. Tasks

##### 1. Title

Title should contain PBI number included in square brackets

![pbi.png](readme_images/pbi.png)

##### 2. Acceptance critera

Acceptance critera should be under tag [AC].

##### 3. Tasks

Tasks should be described as table.

Each task should contain
* Task name (first row, first column) after [Tsk] tag
* Estimate number (first row, second column)
* Description (second row)

