﻿namespace AzureService.Constants
{
    /// <summary>
    /// Azure CLI commands.
    /// </summary>
    public static class AzCommands
    {
        #region public members

        /// <summary>
        /// Login. New browser tab will be opened for it.
        /// </summary>
        public const string Login = "az login";

        /// <summary>
        /// Get token for current user.
        /// </summary>
        public const string GetToken = "az account get-access-token";

        #endregion
    }
}
