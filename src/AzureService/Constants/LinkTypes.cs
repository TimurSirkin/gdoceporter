﻿namespace AzureService.Constants
{
    /// <summary>
    /// Link types for Azure API. E.g. child or parent.
    /// </summary>
    public static class LinkTypes
    {
        #region public members
        /// <summary>
        /// Child link type.
        /// </summary>
        public const string Child = "System.LinkTypes.Hierarchy-Reverse";
        #endregion
    }
}
