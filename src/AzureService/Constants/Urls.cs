﻿namespace AzureService.Constants
{
    /// <summary>
    /// Urls for Azure API.
    /// </summary>
    public static class Urls
    {
        #region public members

        /// <summary>
        /// Create work item.
        /// </summary>
        public const string NewWorkItemUrl =
            "https://dev.azure.com/FossAnalytical/FossConnect/_apis/wit/workitems/$task?api-version=6.0";

        /// <summary>
        /// Update or get work item.
        /// </summary>
        public const string WorkItemUrl =
            "https://dev.azure.com/FossAnalytical/FossConnect/_apis/wit/workitems/{0}?api-version=6.0";

        #endregion
    }
}
