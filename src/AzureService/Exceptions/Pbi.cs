﻿using System;

namespace AzureService.Exceptions
{
    /// <summary>
    /// Document format exception.
    /// </summary>
    public class TaskNotCreatedException : Exception
    {
        public TaskNotCreatedException(string message) : base(message) { }
    }
}