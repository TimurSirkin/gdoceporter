﻿namespace AzureService.Models
{
    /// <summary>
    /// Used for desalinizing of Azure CLI response with access token.
    /// </summary>
    public class AccessTokenResponse
    {
        #region public members

        /// <summary>
        /// Access token.
        /// </summary>
        public string AccessToken { get; set; }

        #endregion
    }
}
