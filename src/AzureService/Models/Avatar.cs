﻿using System.Text.Json.Serialization;

namespace AzureService.Models
{
    /// <summary>
    /// Avatar model received via Azure API.
    /// </summary>
    public class Avatar
    {
        #region public members

        // Link to user avatar.
        [JsonPropertyName("href")]
        public string Href { get; set; }

        #endregion
    }
}