﻿using System.Text.Json.Serialization;

namespace AzureService.Models
{
    /// <summary>
    /// Link contained in request to Azure API.
    /// </summary>
    public class Link
    {
        #region public members

        /// <summary>
        /// Relation type. E.g. parent or child.
        /// </summary>
        [JsonPropertyName("rel")]
        public string Relation { get; set; }

        /// <summary>
        /// Uri of linked item.
        /// </summary>
        [JsonPropertyName("url")]
        public string Url { get; set; }

        #endregion
    }
}
