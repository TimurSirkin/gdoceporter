﻿namespace AzureService.Models
{
    /// <summary>
    /// Product backlog model. Used for update PBI fields.
    /// </summary>
    public class Pbi
    {
        #region public members

        /// <summary>
        /// Identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Acceptance criteria.
        /// </summary>
        public string AcceptanceCriteria { get; set; }

        /// <summary>
        /// PBI end week.
        /// </summary>
        public int EndWeek { get; set; }

        /// <summary>
        /// PBI start week.
        /// </summary>
        public int StartWeek { get; set; }

        /// <summary>
        /// PBI effort.
        /// </summary>
        public int Effort { get; set; }

        #endregion
    }
}
