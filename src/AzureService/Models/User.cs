﻿using System.Text.Json.Serialization;

namespace AzureService.Models
{
    /// <summary>
    /// User model received via Azure API.
    /// </summary>
    public class User
    {
        #region public members

        /// <summary>
        /// User displayed name.
        /// </summary>
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// User url.
        /// </summary>
        [JsonPropertyName("url")]
        public string Url { get; set; }

        /// <summary>
        /// Links.
        /// </summary>
        [JsonPropertyName("_links")]
        public UserLinks Links { get; set; }

        /// <summary>
        /// Identifier.
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// User uniq name. E.g. 'tsirkin@foss.dk'.
        /// </summary>
        [JsonPropertyName("uniqueName")]
        public string UniqueName { get; set; }

        /// <summary>
        /// Avatar image url.
        /// </summary>
        [JsonPropertyName("imageUrl")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Descriptor.
        /// </summary>
        [JsonPropertyName("descriptor")]
        public string Descriptor { get; set; }

        #endregion
    }
}
