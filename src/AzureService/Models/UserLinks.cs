﻿using System.Text.Json.Serialization;

namespace AzureService.Models
{
    /// <summary>
    /// User links model received via Azure API.
    /// </summary>
    public class UserLinks
    {
        #region public members

        /// <summary>
        /// User avatar.
        /// </summary>
        [JsonPropertyName("avatar")]
        public Avatar Avatar { get; set; }

        #endregion
    }
}