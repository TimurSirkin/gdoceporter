﻿namespace AzureService.Models
{
    /// <summary>
    /// WorkItem response model received via Azure API..
    /// </summary>
    public class WorkItemResponse
    {
        #region public members

        /// <summary>
        /// Url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// User info JSON.
        /// </summary>
        public User AssignedTo { get; set; }

        #endregion
    }
}
