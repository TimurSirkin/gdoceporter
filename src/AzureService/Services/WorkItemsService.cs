﻿using System.Text.Json;
using System.Net.Http;
using System.Text;
using AzureService.Models;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AzureService.Helpers;
using AzureService.Constants;
using AzureService.Exceptions;
using Newtonsoft.Json.Linq;
using AzureTask = AzureService.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace AzureService.Services
{
    /// <summary>
    /// Service allows send requests to Azure API. Allows create or update WorkItems.
    /// </summary>
    public static class WorkItemService
    {
        #region private members

        private static readonly HttpClient Client;


        static WorkItemService()
        {
            Client = new HttpClient();
        }

        /// <summary>
        /// Get PBI url and User (to whom PBI is assigned) information.
        /// </summary>
        private static WorkItemResponse GetWorkItemInfo(int pbiId)
        {
            var url = string.Format(Urls.WorkItemUrl, pbiId);
            var response = Client.GetAsync(url);
            response.Wait();

            var task = response.Result.Content.ReadAsStringAsync();
            task.Wait();

            var workItemResponse = new WorkItemResponse
            {
                Url = (string)JObject.Parse(task.Result)["url"],
                AssignedTo = JObject.Parse(task.Result)["fields"]?["System.AssignedTo"]?.ToObject<User>()
            };
            if (workItemResponse.AssignedTo != null)
                workItemResponse.AssignedTo.Links = new UserLinks
                {
                    Avatar = new Avatar
                    {
                        Href = JObject.Parse(task.Result)["fields"]?["System.AssignedTo"]?["_links"]?["avatar"]?["href"]
                            ?.ToString()
                    }
                };

            return workItemResponse;
        }

        /// <summary>
        /// Creates specified Task linked to specified PBI as a child.
        /// </summary>
        private static Task<HttpResponseMessage> CreateWorkItemAsync(AzureTask task, WorkItemResponse workItem)
        {
            var workItemOptions = new List<WorkItemField>
            {
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Title,
                    Value = task.Title
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Activity,
                    Value = task.Activity
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Area,
                    Value = task.Area
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Description,
                    Value = task.Description
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Iteration,
                    Value = task.Iteration
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.RemainingWork,
                    Value = task.RemainingWork.ToString(CultureInfo.InvariantCulture)
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Tags,
                    Value = task.Tags
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.TargetBranch,
                    Value = task.TargetBranch
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Relations,
                    Value = new Link
                    {
                        Relation = LinkTypes.Child,
                        Url = workItem.Url
                    },
                }
            };

            if (workItem.AssignedTo != null)
            {
                workItemOptions.Add(new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.AssignedTo,
                    Value = workItem.AssignedTo
                });
            }

            var json = JsonSerializer.Serialize(workItemOptions, new JsonSerializerOptions {WriteIndented = true});
            var content = new StringContent(json, Encoding.UTF8, "application/json-patch+json");

            return Client.PostAsync(Urls.NewWorkItemUrl, content);
        }
        #endregion

        #region public members

        /// <summary>
        /// Login in Azure and set token.
        /// </summary>
        public static void LoginInAzure()
        {
            CommonHelper.RunCommand(AzCommands.Login);
            var token = CommonHelper.GetToken(CommonHelper.RunCommand(AzCommands.GetToken));
            Client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
        }


        /// <summary>
        /// Create specified WorkItems linked to specified PBI as a child.
        /// </summary>
        public static void CreateWorkItems(List<AzureTask> workItems, int pbiId)
        {
            var workItemResponse = GetWorkItemInfo(pbiId);

            List<Task<HttpResponseMessage>> taskList = new List<Task<HttpResponseMessage>>();
            foreach (var workItem in workItems)
            {
                taskList.Add(CreateWorkItemAsync(workItem, workItemResponse));
            }

            Task.WaitAll(taskList.ToArray());

            if (taskList.Any(t =>
                !t.IsCompletedSuccessfully ||
                !t.Result.IsSuccessStatusCode))
            {
                throw new TaskNotCreatedException("Failed to create tasks.");
            }
        }

        /// <summary>
        /// Update PBI.
        /// </summary>
        public static void UpdateWorkItem(Pbi pbi)
        {
            var workItemOptions = new List<WorkItemField>
            {
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.AcceptanceCriteria,
                    Value = pbi.AcceptanceCriteria
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Effort,
                    Value = pbi.Effort
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.StartWeek,
                    Value = pbi.StartWeek
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.EndWeek,
                    Value = pbi.EndWeek
                }
            };

            var json = JsonSerializer.Serialize(workItemOptions, new JsonSerializerOptions { WriteIndented = true });
            var content = new StringContent(json, Encoding.UTF8, "application/json-patch+json");

            var url = string.Format(Urls.WorkItemUrl, pbi.Id);
            var response = Client.PatchAsync(url, content);
            response.Wait();
        }
        #endregion
    }
}
