﻿namespace GDocExporter.Constants
{
    /// <summary>
    /// Tags placed in Google document. Defines type of section in document.
    /// </summary>
    public static class DocTags
    {
        #region public members

        /// <summary>
        /// Acceptance criteria tag.
        /// </summary>
        public const string AcceptanceCriteria = "[ac]";

        /// <summary>
        /// Task tag.
        /// </summary>
        public const string Task = "[tsk]";

        #endregion
    }
}
