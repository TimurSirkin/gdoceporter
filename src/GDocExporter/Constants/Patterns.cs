﻿namespace GDocExporter.Constants
{
    /// <summary>
    /// Patterns for regex search in Google document.
    /// </summary>
    public static class Patterns
    {
        #region public members

        /// <summary>
        /// Used to search PBI number.
        /// </summary>
        public const string PBI = @"\[\s*PBI\s*\d{6}\s*]";

        #endregion
    }
}
