﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using AzureService.Models;
using GDocExporter.Constants;
using GDocExporter.Exceptions;
using Google.Apis.Docs.v1.Data;

namespace GDocExporter.Converters
{
    /// <summary>
    /// Document converter. Allows convert Google document to Tasks and PBI.
    /// </summary>
    public static class DocConverter
    {
        #region private members

        /// <summary>
        /// Get text from google document elements.
        /// </summary>
        private static string GetTextFromParagraph(IList<StructuralElement> elements)
        {
            return elements.Aggregate("", (current, element) =>
                current + GetTextFromParagraph(element?.Paragraph));
        }

        /// <summary>
        /// Get text from google document elements.
        /// </summary>
        private static string GetTextFromParagraph(Paragraph paragraph)
        {
            if (paragraph == null)
            {
                return string.Empty;
            }

            return paragraph.Elements.Aggregate("", (current, element) =>
                current + element?.TextRun?.Content);
        }

        /// <summary>
        /// Set styles to text. Not implemented yet. TODO.
        /// </summary>
        private static string StyleText(TextRun textRun)
        {
            if (textRun?.Content == null)
            {
                return null;
            }

            string text = textRun.Content;

            if (textRun.TextStyle.Bold ?? false)
            {
                text += "<b>" + text + "</b>";
            }

            if (textRun.TextStyle.Italic ?? false)
            {
                text += "<i>" + text + "</i>";
            }

            if (textRun.TextStyle.Underline ?? false)
            {
                text += "<u>" + text + "</u>";
            }

            return text;
        }

        /// <summary>
        /// Create PBI.
        /// </summary>
        private static Pbi CreatePbi(string text, int id)
        {
            // Get acceptance criteria from text.
            string acceptanceCriteria =
                text.Substring(
                    text.IndexOf(
                        DocTags.AcceptanceCriteria,
                        StringComparison.OrdinalIgnoreCase)
                    + DocTags.AcceptanceCriteria.Length);

            return new Pbi()
            {
                Id = id,
                AcceptanceCriteria = acceptanceCriteria
            };
        }

        /// <summary>
        /// Add default options to task.
        /// </summary>
        private static void AddDefaultOptions(Task task)
        {
            task.Activity = Settings.Default.Activity;
            task.Area = Settings.Default.Area;
            task.Tags = Settings.Default.Tags;
            task.TargetBranch = Settings.Default.TargetBranch;
            task.Iteration = Settings.Default.Iteration;
        }

        /// <summary>
        /// Get PBI identifier from google document elements.
        /// </summary>
        private static int GetPbiId(IList<StructuralElement> elements)
        {
            string text = GetTextFromParagraph(elements);

            Regex rx = new Regex(
                Patterns.PBI,
                RegexOptions.IgnoreCase);

            string pbiId = Regex.Match(rx.Match(text).Value, @"\d{6}").Value;

            return int.Parse(pbiId);
        }

        /// <summary>
        /// Get all tasks from google document elements.
        /// </summary>
        private static List<Task> GetTasks(IEnumerable<StructuralElement> documentElements)
        {
            // Get table from document. It contains all tasks.
            Table taskTable = documentElements.First(e => e.Table != null).Table;
            var tasks = new List<Task>();

            // For each row.
            foreach (var row in taskTable.TableRows)
            {
                // Get text from cell.
                string text = GetTextFromParagraph(row.TableCells[0]?.Content);

                if (text.Contains(DocTags.Task, StringComparison.OrdinalIgnoreCase))
                {
                    // Process task title.

                    // Remove task tag.
                    text = text.Replace(
                            DocTags.Task,
                            "",
                            StringComparison.OrdinalIgnoreCase)
                        .Trim(' ', '\n');

                    var currentWorkItem = new Task
                    {
                        Title = text
                    };

                    // Process task remaining work.

                    // Remove whitespaces.
                    string remainingWorkText = Regex.Replace(
                        input: GetTextFromParagraph(row.TableCells[1].Content[0].Paragraph),
                        pattern: @"\s+",
                        replacement: "");

                    // Replace "," by "."
                    remainingWorkText = Regex.Replace(
                        input: remainingWorkText,
                        pattern: @",",
                        replacement: ".");

                    if (string.IsNullOrEmpty(remainingWorkText))
                    {
                        remainingWorkText = "0.0";
                    }

                    if (!double.TryParse(
                        remainingWorkText,
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture,
                        out double remainingWork))
                    {
                        throw new DocumentFormatException(
                            $"Wrong RemainingWork cell format.\nIncorrect value: {remainingWorkText}");
                    }

                    currentWorkItem.RemainingWork = remainingWork;

                    AddDefaultOptions(currentWorkItem);
                    tasks.Add(currentWorkItem);
                }
                else
                {
                    // Convert description text to HTML format.
                    text = ConvertToHtml(row.TableCells[0].Content);

                    tasks.Last().Description = text;
                }
            }

            return tasks;
        }

        /// <summary>
        /// Convert text to HTML format. It used in Azure.
        /// TODO: Rework list processing and add style processing.
        /// </summary>
        private static string ConvertToHtml(IList<StructuralElement> elements)
        {
            string text = string.Empty;

            // Add lists if exist in document.
            for (int i = 0; i < elements.Count; i++)
            {
                Paragraph paragraph = elements[i]?.Paragraph;
                if (paragraph == null)
                {
                    continue;
                }

                if (paragraph.Bullet?.ListId == null)
                {
                    text += GetTextFromParagraph(paragraph)
                        .Replace("\n", "<br>");
                    continue;
                }

                text += GetList(elements, i, out i);
            }

            return text;
        }

        /// <summary>
        /// Get list from google document elements.
        /// </summary>
        private static string GetList(
            IList<StructuralElement> elements,
            int startListPosition,
            out int endListPosition)
        {
            int currentLevel = 0;
            int i = startListPosition;

            string text = "<ol>";

            while (i < elements.Count &&  elements[i].Paragraph?.Bullet?.ListId != null)
            {
                Paragraph paragraph = elements[i].Paragraph;
                int paragraphLevel = paragraph.Bullet?.NestingLevel ?? 0;

                if (paragraphLevel > currentLevel)
                {
                    text += "<ol>";
                }

                if (paragraphLevel < currentLevel)
                {
                    for (var j = 0; j < currentLevel - paragraphLevel; j++)
                    {
                        text += "</ol>";
                    }
                }

                text += "<li>";
                text += GetTextFromParagraph(paragraph)
                    .Replace("\n", "")
                    .Replace("\v", "<br>");
                text += "</li>";

                currentLevel = paragraphLevel;
                i++;
            }

            for (var j = 0; j < currentLevel + 1; j++)
            {
                text += "</ol>";
            }

            endListPosition = i-1;
            return text;
        }

        #endregion

        #region public members

        /// <summary>
        /// Convert document to tasks and parent PBI.
        /// </summary>
        public static (Pbi Pbi, List<Task> Tasks) Convert(Document document)
        {
            Pbi pbi =
                CreatePbi(
                    ConvertToHtml(document.Body.Content),
                    GetPbiId(document.Body.Content));

            List<Task> tasks = GetTasks(document.Body.Content);

            return (pbi, tasks);
        }

        #endregion
    }
}
