﻿using System;

namespace GDocExporter.Exceptions
{
    /// <summary>
    /// Document format exception.
    /// </summary>
    public class DocumentFormatException : Exception
    {
        public DocumentFormatException(string message) : base(message) { }
    }
}
