﻿using System;

namespace GDocService.Exceptions
{
    /// <summary>
    /// Document format exception.
    /// </summary>
    public class DocumentNotFoundException : Exception
    {
        public DocumentNotFoundException(string message) : base(message) { }
    }
}
